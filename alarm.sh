#!/bin/sh

# simple alarm/timer script in POSIX sh
# dependencies: sox

# Copyright (C) 2022 spitemim

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

progname="$(basename "$0")"

usage() {
	cat << EOF
usage: $progname [-h] [-p filename [-l]] [-c shellcmd] [-n] NUMBER[SUFFIX]...
Display timer for n seconds, make alarm sound.

Uses sleep(1)-like syntax: SUFFIX may be 's' for seconds (assumed if no
suffix given), 'm' for minutes, 'h' for hours, or 'd' for days. NUMBER
should be an integer.

WARNING: The command line interface is highly subject to change.
Script with caution.

-h		display help message

-p filename	play filename when time is up
		default: make beeping noise

-l		loop alarm sound. only applicable when
		an alternate alarm sound is set (see -p option)

-n		don't clear line when printing elapsed time

-c shellcmd	run shellcmd with /bin/sh when timer is up
EOF
}

secs2hms() {
	if [ $# -ne 1 ]
	then
		return
	fi

	secs="$1"

	printf '%02dh:%02dm:%02ds' $((secs / 3600)) \
		$((secs % 3600 / 60)) $((secs % 60))
}

secs=0
sound=""
customcmd=""
clearlines=true
loop=false

if [ $# -lt 1 ]
then
	usage
	exit
fi

while [ "$1" ]
do
	case "$1" in
		-h)
			usage
			exit
			;;
		-p)
			shift
			sound="$1"
			shift
			continue
			;;
		-l)
			if [ "$sound" ]
			then
				loop=true
			else
				printf "%s: -l: only applicale when -p is set\n" "$progname"
				usage
				exit
			fi

			shift
			continue
			;;
		-n)
			clearlines=false
			shift
			continue
			;;
		-c)
			shift
			customcmd="$1"
			shift
			continue
			;;
	esac

	if ! printf "%s" "$1" | grep -Eoq '^[0-9]+[dhms]?$'
	then
		printf "%s: invalid time interval '%s'\n" "$progname" "$1"
		usage
		exit
	fi

	timetype="$(printf "%s" "$1" | grep -Eo "[dhms]$")"
	duration="$(printf "%s" "$1" | grep -Eo "^[0-9]+")"

	case "$timetype" in
		d)      secs=$((secs + duration * 60 * 60 * 24)) ;;
		h)      secs=$((secs + duration * 60 * 60)) ;;
		m)      secs=$((secs + duration * 60)) ;;
		s | "") secs=$((secs + duration)) ;;
	esac

	shift
done

elapsed=0

# when we use \033[A, we don't clear the terminal line
[ "$clearlines" = true ] && printf '\n'

while [ $elapsed -ne $secs ]
do
	# up 1 line, clear line, move cursor to beginning of line
	[ "$clearlines" = true ] && printf '\033[A\033[2K\r'
	printf "%s / %s\n" "$(secs2hms "$elapsed")" "$(secs2hms "$secs")"

	sleep 1
	elapsed=$((elapsed + 1))
done

[ "$clearlines" = true ] && printf '\033[A\033[2K\r'
printf "%s / %s (Done!)\n" "$(secs2hms "$elapsed")" "$(secs2hms "$secs")"

if [ "$customcmd" ]
then
	printf "Running custom command: %s\n" "$customcmd"
	if ! sh -c "$customcmd"
	then
		printf "Custom command failed with status %d\n" "$?"
	fi
fi

printf "Press CTRL+C to stop sound\n"

if [ "$sound" ]
then
	if [ "$loop" = true ]
	then
		while true; do play -q "$sound"; done
	else
		play -q "$sound"
	fi
else
	while true
	do
		play -qn -c1 synth 0.5 square 440 || break
		sleep 0.2
	done
fi
