# alarm.sh -- simple alarm/timer script in POSIX sh

**alarm.sh** is a simple (as simple as possible, but not simpler) timer/alarm script written in POSIX sh. You can use it to wake yourself up in the morning, to work out, to pace yourself when taking on a big project, or anything else you can think of that needs a timer.

## usage

**WARNING!** The command line interface is subject to change. Script with
caution for now.

For full documentation, see `alarm.sh -h`.

To set a 10 minute timer:

    alarm.sh 10m

The script uses `sleep(1)`-like syntax to specify duration. Each argument must
be an integer followed by an optional 1-letter suffix - s for seconds, m for
minutes, h for hours, and d for days. Numbers with no suffix are assumed to be
seconds. The total duration is the sum of each argument.

  * `alarm.sh 3h 10m 5s` - set timer for 3 hours, 10 minutes, and 5 seconds
  * `alarm.sh 10m 3h 5d` - the order of the arguments doesn't matter!

By default, alarm.sh will play a beeping sound when the timer is up. Use `-p`
to specify a sound file to play.

    alarm.sh -p my_favorite_song.mp3 10m

You can combine this with the `-l` argument to loop the specified audio file.
This option is only applicable after the `-p` option.

    alarm.sh -p my_favorite_song.mp3 -l 10m

You can use the `-c` flag to specify a shell command that should run when the
timer ends. For example, to boost your volume to 100% before running the alarm
sound:

    alarm.sh -c "pamixer -u; pamixer --set-volume 100" 5h

alarm.sh provides a "time elapsed / total duration" status line. Looks like
this:

    $ alarm.sh 10m
    00h:02m:07s / 00h:10m:00s

The script uses ANSI escape codes to clear the line and re-print the status
info, to keep the interface minimal. Use `-n` to disable this feature.

    $ alarm.sh -n 10m
    00h:00m:00s / 00h:10m:00s
    0h:00m:01s / 00h:10m:00s
    00h:00m:02s / 00h:10m:00s
    ...
    00h:10m:00s / 00h:10m:00s (Done!)
    Press CTRL+C to stop sound

## installation

### dependencies

This script depends on `sox`, for the `play` command.

### instructions

Clone this repo and copy `alarm.sh` somewhere in your $PATH.

    git clone https://git.spitemim.xyz/alarm.sh
    cd alarm.sh
    chmod +x ./alarm.sh
    sudo mv alarm.sh /usr/bin

## todo

  * floating point support (like `sleep(1)`)
  * <strike>add argument for specifying shell commands that will run when the timer ends</strike>
  * add support for setting the alarm with a date string like "9 am tomorrow" or "5 minutes"
  * add other simple interfaces
  * long arguments, maybe?

## license

This program is free software, licensed under the GNU GPLv3. [See the LICENSE](LICENSE) for details.
